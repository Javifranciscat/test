create database if not exists bookapp;
use bookapp;

create table if not exists category(
    id int zerofill auto_increment,
    name varchar(30) not null,
    description varchar(30),
    primary key(id)
);

create table if not exists book(
    id int zerofill auto_increment,
    Title varchar(30) not null,
    id_category int unsigned not null,
    author varchar(30) not null,
    editorial varchar(30) not null,
    copy int unsigned,
    primary key(id),
    foreign key(id_category) references category(id) on delete cascade
);

select book.id, book.title,category.name,
       book.author, book.editorial, book.copy
from book
join category 
on book.id_category = category.id;