package book.app;

import book.config.MysqlConnection;
import book.controller.BookController;
import book.controller.CategoryController;
import book.model.BookModel;
import book.model.CategoryModel;
import book.model.DatabaseModel;
import book.view.View;
import java.sql.Connection;

public class Bookapp {
    Connection connection = MysqlConnection.getConnection();

    public static void main(String[] args) {
        Connection connection = MysqlConnection.getConnection();
        DatabaseModel dataBase = new DatabaseModel(connection);
        
        View view = new View();
        
        CategoryModel cModel = new CategoryModel(connection);
        CategoryController cController = new CategoryController(view, cModel);
        cController.start();
        
        BookModel bModel = new BookModel(connection);
        BookController bController = new BookController(view, bModel);
        bController.start();
    }
    
}
