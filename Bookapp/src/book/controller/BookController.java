package book.controller;

import book.model.BookModel;
import book.view.Alert;
import book.view.View;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;

public class BookController extends Controller{
    
    private final View view;
    private final BookModel model;

    public BookController(View view, BookModel model) {
        this.view = view;
        this.model = model;
    }
    
    public void start(){
        this.view.jbAddBook.addActionListener(this);
        this.view.jbSaveBook.addActionListener(this);
        this.view.jbUpdateBook.addActionListener(this);
        this.view.jbConfirmUpdateBook.addActionListener(this);
        this.view.jbDeleteBook.addActionListener(this);
        this.view.jtBook.addMouseListener(this);
        this.getAllTable();
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == view.jbAddBook)this.addBookButton();
        if(e.getSource() == view.jbSaveBook)this.addBookProcess();
        if(e.getSource() == view.jbUpdateBook)this.updateBookButton();
        if(e.getSource() == view.jbConfirmUpdateBook)this.updateBookProcess();
        if(e.getSource() == view.jbDeleteBook)this.deleteBookProcess();
    }
    
    @Override
    public void mouseClicked(MouseEvent e){
        if(e.getSource() == view.jtBook)this.selectBookTable();
    }

    public void addBookButton() {
        this.getCategoryComboBox();
        try {
            if(this.model.getCategories().getSize() == 0){
                String title = "Alerta";
                String mesagge = "No existen aun categorias\n"
                        + "¿Desea agregar una?";
                int option = Alert.mesaggeAlert(this.view, title, mesagge);
                switch(option){
                    case 0:this.view.setPopWindow(this.view.jpAddCategory);
                    break;
                    case 1:
                    break;
                }
                
            }else{
                this.view.setPopWindow(view.jpAddBook);
            }
        }catch(SQLException ex){
            System.out.println("Error: " + ex);
        }    
    }

    public void addBookProcess() {
        
        this.getCategoryComboBox();

        this.model.setTitle(view.jtBookTitle.getText());
        this.model.setAuthor(view.jtBookAuthor.getText());
        this.model.setCategory((String) view.jcBookCategory.getSelectedItem());
        this.model.setEditorial(view.jtBookEditorial.getText());
        this.model.setCopy(view.jtBookCopy.getText());

        try {
            this.model.post();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        this.view.closePopWindow();
            
        
        this.getAllTable();
    }

    public void updateBookButton() {
        this.getCategoryComboBox();
        this.view.jtNewTitle.setText(this.model.getTitle());
        this.view.jtNewAuthor.setText(this.model.getAuthor());
        this.view.jcNewCategory.setSelectedItem(this.model.getCategory());
        this.view.jtNewEditorial.setText(this.model.getEditorial());
        this.view.jtNewCopy.setText(this.model.getCopy());
        this.view.setPopWindow(this.view.jpUpdateBook);
    }

    public void updateBookProcess() {
        this.model.setTitle(this.view.jtNewTitle.getText());
        this.model.setAuthor(this.view.jtNewAuthor.getText());
        this.model.setCategory((String) this.view.jcNewCategory.getSelectedItem());
        this.model.setEditorial(this.view.jtNewEditorial.getText());
        this.model.setCopy(this.view.jtNewCopy.getText());
        
        try {
            this.model.put();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } 
        this.view.closePopWindow();
        this.getAllTable();
    }

    public void deleteBookProcess() {
        String title = "Borrar";
        String mesagge = "¿Desea eliminar "
                + this.model.getTitle() + "?";
        int option = Alert.mesaggeAlert(this.view, title, mesagge);
        switch(option){
            case 0: {
                try {
                    this.model.delete();
                } catch (SQLException ex) {
                    System.out.println("Error: " + ex);
                }
            }
            break;
            case 1:break;
        }
        this.getAllTable();
        }
    
    public void getAllTable() {
        try {
            this.view.jtBook.setModel(this.model.getAll());
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
    }
    
    public void getCategoryComboBox(){
        try {
            this.view.jcBookCategory.setModel(this.model.getCategories());
            this.view.jcNewCategory.setModel(this.model.getCategories());
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
    }
    
    public void selectBookTable(){
        JTable table = this.view.jtBook;
        Object id = table.getValueAt(table.getSelectedRow(), 0);
        Object title = table.getValueAt(table.getSelectedRow(), 1);
        Object category = table.getValueAt(table.getSelectedRow(), 2);
        Object author = table.getValueAt(table.getSelectedRow(), 3);
        Object editorial = table.getValueAt(table.getSelectedRow(), 4);
        Object copy = table.getValueAt(table.getSelectedRow(), 5);
        
        this.model.setId(id.toString());
        this.model.setTitle(title.toString());
        this.model.setCategory(category.toString());
        this.model.setAuthor(author.toString());
        this.model.setEditorial(editorial.toString());
        this.model.setCopy(copy.toString());
    }
}
