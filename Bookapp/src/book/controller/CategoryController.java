package book.controller;

import book.model.CategoryModel;
import book.view.Alert;
import book.view.View;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import javax.swing.JTable;

public class CategoryController extends Controller{
    
    private View view;
    private CategoryModel model;

    public CategoryController(View view, CategoryModel model) {
        this.view = view;
        this.model = model;
    }
    
    public void start(){
        this.view.setTitle("Book");
        this.view.setVisible(true);
        this.view.setLocationRelativeTo(null);
        
        this.view.jmInit.addMouseListener(this);
        this.view.jmCategory.addMouseListener(this);
        this.view.jmBook.addMouseListener(this);
        this.view.jbAddCategory.addActionListener(this);
        this.view.jbSaveCategory.addActionListener(this);
        this.view.jbUpdateCategory.addActionListener(this);
        this.view.jbConfirmUpdate.addActionListener(this);
        this.view.jbDeleteCategory.addActionListener(this);
        this.view.jtCategory.addMouseListener(this);
        
        getAllTable();
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == view.jbAddCategory)this.addCategoryButton();
        if(e.getSource() == view.jbSaveCategory)this.addCategoryProcess();
        if(e.getSource() == view.jbUpdateCategory)this.updateCategoryButton();
        if(e.getSource() == view.jbConfirmUpdate)this.updateCategoryProcess();
        if(e.getSource() == view.jbDeleteCategory)this.deleteCategoryProcess();
    }
    
    @Override
    public void mouseClicked(MouseEvent e){
        if(e.getSource() == view.jmInit)view.reloadPrincipalPanel(view.jpInit);
        if(e.getSource() == view.jmCategory)view.reloadPrincipalPanel(view.jpCategory);
        if(e.getSource() == view.jmBook)view.reloadPrincipalPanel(view.jpBook);
        if(e.getSource() == view.jtCategory)this.selectCategoryTable();
    }
    
    public void addCategoryButton(){
        this.view.setPopWindow(this.view.jpAddCategory);
    }
    
    public void addCategoryProcess(){
        model.setName(view.jtCategoryName.getText());
        model.setDescription(view.jtCategoryDescription.getText());
        
        try {
            model.post();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        this.view.closePopWindow();
        this.getAllTable();
    }
    
    public void updateCategoryButton(){
        this.view.jtNewName.setText(this.model.getName());
        this.view.jtNewDescription.setText(this.model.getDescription());
        this.view.setPopWindow(this.view.jpUpdateCategory);
    }
    
    public void updateCategoryProcess(){
        this.model.setName(this.view.jtNewName.getText());
        this.model.setDescription(this.view.jtNewDescription.getText());
        
        try {
            this.model.put();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        
        this.view.closePopWindow();
        this.getAllTable();
    }
    
    public void deleteCategoryProcess(){
        String title = "Borrar";
        String mesagge = "¿Desea eliminar "
                + this.model.getName() + "?";
        int option = Alert.mesaggeAlert(this.view, title, mesagge);
        switch(option){
            case 0: {
                try {
                    this.model.delete();
                } catch (SQLException ex) {
                    System.out.println("Error: " + ex);
                }
            }
            break;
            case 1:break;
        }
        this.getAllTable();
    }

    public void getAllTable() {
        try {
            this.view.jtCategory.setModel(this.model.getAll());
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
    }
    
    public void selectCategoryTable(){
        JTable table = this.view.jtCategory;
        Object id = table.getValueAt(table.getSelectedRow(), 0);
        Object name = table.getValueAt(table.getSelectedRow(), 1);
        Object description = table.getValueAt(table.getSelectedRow(), 2);
        
        this.model.setId(id.toString());
        this.model.setName(name.toString());
        this.model.setDescription(description.toString());
    }
}
