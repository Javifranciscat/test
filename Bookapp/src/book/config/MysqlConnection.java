package book.config;

import java.sql.Connection;
import java.sql.DriverManager;

public class MysqlConnection {
    
    public static Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName(Environment.DRIVER); 
            connection = (Connection) DriverManager.getConnection(
                Environment.MYSQL_URL,
                Environment.USER_NAME,
                Environment.USER_PASSWORD
            );
        } catch (ClassNotFoundException e) {
            System.out.println("Error: " + e);
        } catch (java.sql.SQLException e) {
            System.out.println("Sql Error: " + e);
        }
        return connection;    
    }
}
