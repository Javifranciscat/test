package book.config;

public class Environment {
    public static final String PORT = ":3306";
    public static final String HOST = "localhost";
    public static final String DATABASE_NAME = "bookapp";
    public static final String STANDAR = "?autoReconnect=true&useSSL=false";
    public static final String MYSQL_URL = "jdbc:mysql://"
            + HOST
            + PORT + STANDAR;
    public static final String USER_NAME = "javiera";
    public static final String USER_PASSWORD = "pc-cito";
    public static final String DRIVER = "com.mysql.jdbc.Driver";
}
