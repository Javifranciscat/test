package book.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseModel {
    
    public DatabaseModel(Connection connection){
        
        String query1, query2, query3, query4;
        PreparedStatement ps1, ps2, ps3, ps4;
        
        query1 = "create database if not exists bookapp;";
        query2 = "use bookapp;";
        query3 = "create table if not exists category(" +
                 "     id int zerofill auto_increment," +
                 "     name varchar(30) not null," +
                 "     description varchar(30)," +
                 "     primary key(id)" +
                 " );";
        query4 = "create table if not exists book(" +
                 "     id int zerofill auto_increment," +
                 "     Title varchar(30) not null," +
                 "     id_category int unsigned not null," +
                 "     author varchar(30) not null," +
                 "     editorial varchar(30) not null," +
                 "     copy int unsigned," +
                 "     primary key(id)," +
                 "     foreign key(id_category) references category(id) on delete cascade" +
                 " );";
        
        try {
            ps1 = connection.prepareStatement(query1);
            ps2 = connection.prepareStatement(query2);
            ps3 = connection.prepareStatement(query3);
            ps4 = connection.prepareStatement(query4);
            
            int result1 = ps1.executeUpdate();
            int result2 = ps2.executeUpdate();
            int result3 = ps3.executeUpdate();
            int result4 = ps4.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
    }
}
