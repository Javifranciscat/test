package book.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public class BookModel implements Model{
    
    private String id;
    private String title;
    private String category;
    private String author;
    private String editorial;
    private String copy;
    private Connection connection;
    
    public BookModel(Connection connection){
        this.connection = connection;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getCopy() {
        return copy;
    }

    public void setCopy(String copy) {
        this.copy = copy;
    }

    @Override
    public void post() throws SQLException {
        String query;
        PreparedStatement ps;
        
        query = "insert into book values("
                + " null,"
                + " ?,"
                + " (select id from category where name = ?),"
                + " ?,"
                + " ?,"
                + " ?);";
        
        ps = connection.prepareStatement(query);
        ps.setString(1, this.title);
        ps.setString(2, this.category);
        ps.setString(3, this.author);
        ps.setString(4, this.editorial);
        ps.setString(5, this.copy);
        
        int result = ps.executeUpdate();
    }

    @Override
    public DefaultTableModel getAll() throws SQLException {
        DefaultTableModel table = new DefaultTableModel();
        
        String query;
        PreparedStatement ps;
        
        query = "select book.id, book.title, category.name, " +
                "       book.author, book.editorial, book.copy " +
                "from book " +
                "join category " +
                "on book.id_category = category.id;";
        
        table.addColumn("ID");
        table.addColumn("Titulo");
        table.addColumn("Categoria");
        table.addColumn("Autor");
        table.addColumn("Editorial");
        table.addColumn("Ejemplares");
        
        int num = table.getColumnCount();
        
        ps = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
            Object[] row = new Object[num];

            for(int i = 0; i < num; i++){
                row[i] = rs.getObject(i + 1);
            }

            table.addRow(row);    
        }
        
        return table;
    }

    @Override
    public void delete() throws SQLException {
        
        String query;
        PreparedStatement ps;
        
        query = "delete from book where id = ?;";
        ps = connection.prepareStatement(query);
        
        ps.setString(1, this.id);
        
        int result = ps.executeUpdate();
    }

    @Override
    public void put() throws SQLException {
        
        String query;
        PreparedStatement ps;
        
        query = "update book set"
                + " Title = ?,"
                + " id_category = (select id from category where name = ?),"
                + " author = ?,"
                + " editorial = ?,"
                + " copy = ?"
                + " where id = ?;";
        
        ps = connection.prepareStatement(query);
        ps.setString(1, this.title);
        ps.setString(2, this.category);
        ps.setString(3, this.author);
        ps.setString(4, this.editorial);
        ps.setString(5, this.copy);
        ps.setString(6, this.id);
        
        int result = ps.executeUpdate();
    }
    
    public DefaultComboBoxModel getCategories() throws SQLException{
        DefaultComboBoxModel<String> comboBox = new DefaultComboBoxModel<>();
        
        String query;
        PreparedStatement ps;
        int num = 0;
        
        query = "select name from category;";
        ps = connection.prepareStatement(query);
        
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            String categoria = (String) rs.getObject(1);
            comboBox.addElement(categoria);
        }
        
        return comboBox;
    }
}
