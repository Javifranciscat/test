package book.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

public class CategoryModel implements Model{
    
    private String id;
    private String name;
    private String description;
    public Connection connection;
    
    public CategoryModel(Connection connection){
        this.connection = connection;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void post() throws SQLException {
        
        String query;
        PreparedStatement ps;
        
        query = "insert into category values("
                + " null,"
                + " ?,"
                + " ?);";
        
        ps = connection.prepareStatement(query);
        ps.setString(1, this.getName());
        ps.setString(2, this.getDescription());
        
        int result = ps.executeUpdate();
    }

    @Override
    public DefaultTableModel getAll() throws SQLException {
        DefaultTableModel table = new DefaultTableModel();
        
        String query;
        PreparedStatement ps;
        
        query = "select id, name, description from category;";
        table.addColumn("ID");
        table.addColumn("Nombre");
        table.addColumn("Descripcion");
        
        int num = table.getColumnCount();
            
        ps = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
            
        while(rs.next()){
            Object[] row = new Object[num];

            for(int i = 0; i < num; i++){
                row[i] = rs.getObject(i + 1);
            }

            table.addRow(row);    
        }
        return table;
    }

    @Override
    public void delete() throws SQLException {
        
        String query;
        PreparedStatement ps;
        
        query = "delete from category where id = ?";
        
        ps = connection.prepareStatement(query);
        ps.setString(1, this.getId());
        
        int result = ps.executeUpdate();
    }

    @Override
    public void put() throws SQLException {
        
        String query;
        PreparedStatement ps;
        
        query = "update category set"
                + " name = ?,"
                + " description = ?"
                + " where id = ?;";
        
        ps = connection.prepareStatement(query);
        ps.setString(1, this.getName());
        ps.setString(2, this.getDescription());
        ps.setString(3, this.getId());
        
        int result = ps.executeUpdate();
    }
    
}
