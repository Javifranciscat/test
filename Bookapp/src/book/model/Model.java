package book.model;

import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

public interface Model {
    
    public void post() throws SQLException;
    
    public DefaultTableModel getAll() throws SQLException;
    
    public void delete() throws SQLException;
    
    public void put() throws SQLException;
}
